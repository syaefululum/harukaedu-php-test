<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Main</title>
        {!! Html::style('assets/css/bootstrap.min.css') !!}
        <style>
            .form-horizontal {
                margin-top: 50px;
            }
        </style>
		
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-2">
                    {!! Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files'=>true)) !!}
                        <div class="form-group">
							{!! Form::label('coordinate', 'Coordinate', ['class' => 'control-label col-md-2']) !!}
							<div class="col-md-5">
								{!! Form::text('longitude', null, ['class' => 'form-control', 'placeholder' => 'Longitude']) !!}
								@if ($errors->has('longitude'))<p style="color:red;">{!!$errors->first('longitude')!!}</p>@endif
								<span>Example : -6.243376</span>
							</div>
							<div class="col-md-5">
								{!! Form::text('latitude', null, ['class' => 'form-control', 'placeholder' => 'Latitude']) !!}
								@if ($errors->has('latitude'))<p style="color:red;">{!!$errors->first('latitude')!!}</p>@endif
								<span>Example : 106.784425</span>
							</div>
                        </div>
						<div class="form-group">
                            {!! Form::label('distance', 'Distance (KM)', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::text('distance', null, ['class' => 'form-control', 'placeholder' => 'Distance (KM)']) !!}
								@if ($errors->has('distance'))<p style="color:red;">{!!$errors->first('distance')!!}</p>@endif
                            </div>
                        </div>
 
                        <div class="form-group">
                            {!! Form::label('file','File' , ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                {!! Form::file('csvFile', ['id' => 'csvFile']) !!}
								@if ($errors->has('csvFile'))<p style="color:red;">{!!$errors->first('csvFile')!!}</p>@endif
                            </div>
                        </div>
 
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                {!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => '']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
			<div class="row">
			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-condensed table-bordered" id="articles">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Distance (KM)</th>      
					</tr>
				</thead>
				<tbody>
					@foreach ($result as $row)
						<tr>
						<td>{{ $row['id'] }}</td>
						<td>{{ $row['name'] }}</td>
						<td>{{ $row['distance'] }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			
			
			</div>
        </div>
 
        <!-- jQuery library -->
		{!! Html::script('assets/js/jquery.min.js') !!}

		{!! Html::script('assets/js/bootstrap.min.js') !!}
		<script type="text/javascript">
		$.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});
		</script>
		{!! Html::script('assets/js/main.js') !!}
		
    </body>
</html>