<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MainController extends Controller
{
    public function index()
	{
		return view('main',['result' => Array()]);
	}
	
	function process(Request $request){
		//https://github.com/thephpleague/geotools
		
		$result = Array();
		$this->validate($request, [
			'latitude' => 'required',
			'longitude' => 'required',
			'distance' => 'required|numeric',
			'csvFile' => 'required|file'
		]);
		
		$file = $_FILES['csvFile'];
        $fileName = $request->file('csvFile')->getClientOriginalName();
		$ext = $request->file('csvFile')->getClientOriginalExtension();
		$path = "public/file/";
       
		if($ext == 'csv') {
			$request->file('csvFile')->move($path , $fileName);
			
			$geotools = new \League\Geotools\Geotools();
			$coordA   = new \League\Geotools\Coordinate\Coordinate([$request->input('longitude'), $request->input('latitude')]);
			
			$fileData = public_path('file/' . $fileName);
			$customerArr = $this->csvToArray($fileData);

			foreach($customerArr as $row){
				$coordB   = new \League\Geotools\Coordinate\Coordinate([$row['latitude'], $row['longitude']]);
				$distance = $geotools->distance()->setFrom($coordA)->setTo($coordB);	
				if($distance->in('km')->haversine() <= $request->input('distance')){
					$row['distance'] = round(floatval($distance->in('km')->haversine()),2);
					$result[] = $row;
				}
			}
			
			usort($result, function($a, $b){
				//return $a['distance'] <=> $b['distance'];
				if ($a['distance'] == $b['distance']) return 0;
				return $a['distance'] < $b['distance'] ? -1 : 1;
			});
		}
		return view('main',['result' => $result]);
	}
	
	
	function csvToArray($filename = '', $delimiter = ',')
	{
		if (!file_exists($filename) || !is_readable($filename))
			return false;

		$header = null;
		$data = array();
		if (($handle = fopen($filename, 'r')) !== false)
		{
			while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
			{
				if (!$header)
					$header = $row;
				else
					$data[] = array_combine($header, $row);
			}
			fclose($handle);
		}

		return $data;
	}
}
