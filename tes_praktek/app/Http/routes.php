<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    //return view('main',['result' => Array()]);
	return redirect()->action('MainController@index');
});

Route::get('main', [
    'uses' => 'MainController@index',
]);

Route::post('main', 'MainController@process');